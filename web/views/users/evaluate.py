import logging

from django.contrib import messages
from django.views.generic.base import TemplateView
from django.shortcuts import redirect, render
from django.contrib.auth.mixins import PermissionRequiredMixin

from web.utils import get_user, get_course, add_evaluation, get_evaluation
from web.form import EvaluateForm


logger = logging.getLogger(__name__)


class UserEvaluate(PermissionRequiredMixin, TemplateView):

    template_name = "web/users/evaluate.html"
    permission_required = 'web.view_evaluation'

    def handle_no_permission(self):
        messages.error(self.request, "You don't have permission to access this page")
        return render(self.request, 'permission_denied.html')

    def get(self, request, **kwargs):
        context = super().get_context_data(**kwargs)
        student_id = kwargs.get('user_id')
        course_id = kwargs.get('course_id')

        try:
            student = get_user(employee_id=student_id)[0]
            course = get_course(id=course_id)[0]
            eva = get_evaluation(student_id=student_id, course_name=course[1])
            if eva:
                context['evaluation'] = eva[0]
            context['form'] = EvaluateForm(initial={'student': '{} ({})'.format(student[12], student[11]), 'course': course[1]})
            if int(student[13]) != 0:
                messages.error(request, 'Can evaluate student only!')
                return redirect('web:course_management', course_id=course_id)
        except IndexError:
            messages.error(request, 'Invalid Course or Student')
            return redirect('web:course_management', course_id=course_id)

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        logger.info('===== Start adding evaluation for user =====')
        student_id = kwargs.get('user_id')
        course_id = kwargs.get('course_id')

        evaluation = request.POST.get('evaluation')
        assessor_id = request.user.employee_id
        assessor_name = request.user.full_name

        try:
            add_evaluation(student_id, assessor_id, assessor_name, course_id, evaluation)
        except IndexError as e :
            messages.error(request, 'Data not found')
            logger.error('===== {} ====='.format(e))
            return redirect('web:course_management', course_id=course_id)

        logger.info('===== Finish adding evaluation for user =====')
        messages.success(request, 'Evaluated successfully')
        return redirect('web:course_management', course_id=course_id)
