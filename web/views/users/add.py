import logging

from django.contrib import messages
from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.db import IntegrityError
from django.contrib.auth.mixins import PermissionRequiredMixin

from web.utils import add_user
from web.form import UserAddForm


logger = logging.getLogger(__name__)


class UserAdd(PermissionRequiredMixin, TemplateView):

    template_name = "web/users/add.html"
    permission_required = 'web.add_user'

    def handle_no_permission(self):
        messages.error(self.request, "You don't have permission to access this page")
        return render(self.request, 'permission_denied.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = UserAddForm()
        return context

    def post(self, request, *args, **kwargs):
        logger.info('===== Start adding new user =====')
        context = super().get_context_data(**kwargs)
        name = request.POST.get('name')
        id = request.POST.get('id')
        department = request.POST.get('department')
        account = request.POST.get('account')
        password = request.POST.get('password')
        role = request.POST.get('role')

        form = {'id': id, 'name': name, 'department': department, 'account': account, 'role': role}
        logger.info(form)

        try:
            add_user(password=password, full_name=name, employee_id=id, department=department, username=account, role=role)
        except IntegrityError as e:
            logger.error('===== {} ====='.format(e))
            messages.error(request, 'Duplicated Employee ID or Account')

            context['form'] = UserAddForm(initial=form)
            return render(request, self.template_name, context=context)

        logger.info('===== Finish adding new user =====')
        messages.success(request, 'User {} has been added successfully'.format(id))
        return redirect('web:user_list')
