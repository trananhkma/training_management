import logging

from django.contrib import messages
from django.views.generic.base import TemplateView
from django.shortcuts import render
from django.contrib.auth.mixins import PermissionRequiredMixin

from web.form import UserForm
from web.utils import get_user


logger = logging.getLogger(__name__)


class UserView(PermissionRequiredMixin, TemplateView):

    template_name = "web/users/list.html"
    permission_required = 'web.manage_users'

    def handle_no_permission(self):
        messages.error(self.request, "You don't have permission to access this page")
        return render(self.request, 'permission_denied.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = UserForm()
        users = get_user()
        context['users'] = users
        return context

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        name = request.POST.get('name')
        id = request.POST.get('id')

        logger.info('===== Searching user with {} ====='.format({'id': id, 'name': name}))

        users = get_user(full_name__icontains=name, employee_id=id)
        context['users'] = users

        logger.info('Records found: {}'.format(len(users)))

        form = UserForm(initial={'name': name, 'id': id})
        context['form'] = form

        logger.info('===== Finish Searching user =====')

        return render(request, self.template_name, context=context)
