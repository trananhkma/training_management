import logging

from django.contrib import messages
from django.http import HttpResponseForbidden
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

from web.utils import remove_user


logger = logging.getLogger(__name__)


def delete_user(request, user_id):
    logger.info('==== Start deleting user {} ====='.format(user_id))
    if not request.user.has_perm('web.delete_user'):
        logger.error('==== {} ====='.format('Permission Denied'))
        return HttpResponseForbidden('<H1>Permission denied!</H1>')
    try:
        remove_user(employee_id=user_id)
        logger.info('==== Finish deleting user =====')
        messages.success(request, 'Deleted user successfully')
    except ObjectDoesNotExist as e:
        logger.error('==== {} ====='.format(e))
        messages.error(request, 'User not found')
    return JsonResponse({})
