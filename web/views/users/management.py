import logging

from django.views.generic.base import TemplateView
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin

from web.form import UserManagementForm
from web.utils import get_user, get_course, get_courses_from_user, update_user


logger = logging.getLogger(__name__)


class UserManagement(PermissionRequiredMixin, TemplateView):

    template_name = "web/users/management.html"
    permission_required = 'web.change_user'

    def handle_no_permission(self):
        messages.error(self.request, "You don't have permission to access this page")
        return render(self.request, 'permission_denied.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_id = kwargs['user_id']
        user = get_user(employee_id=user_id)

        if not user:
            messages.error(self.request, 'user not found')
            return redirect('web:user_list')

        user = user[0]
        form = UserManagementForm(initial={'name': user[12], 'id': user_id, 'department': user[14]})
        courses = get_course()
        joined_courses = get_courses_from_user(user_id)
        context['form'] = form
        context['courses'] = courses
        context['joined_courses'] = joined_courses
        return context

    def post(self, request, *args, **kwargs):
        user_id = kwargs['user_id']
        logger.info('===== Updating user {} ====='.format(user_id))

        department = request.POST.get('department')
        courses = request.POST.getlist('courses')

        logger.info('Data: {}'.format({'department': department, 'courses': courses}))

        update_user(user_id, department, courses)

        messages.success(request, 'Updated data successfully')
        logger.info('===== Finish updating user =====')
        return redirect('web:user_list')
