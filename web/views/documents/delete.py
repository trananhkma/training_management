import logging

from django.contrib import messages
from django.http import HttpResponseForbidden
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

from web.utils import remove_document


logger = logging.getLogger(__name__)


def delete_document(request, doc_id):
    logger.info('==== Start deleting document {} ====='.format(doc_id))
    if not request.user.has_perm('web.delete_documents'):
        logger.error('==== {} ====='.format('Permission Denied'))
        return HttpResponseForbidden('<H1>Permission denied!</H1>')
    try:
        remove_document(id=doc_id)
        logger.info('==== Finish deleting document =====')
        messages.success(request, 'Deleted document successfully')
    except ObjectDoesNotExist as e:
        logger.error('==== {} ====='.format(e))
        messages.error(request, 'Document not found')
    return JsonResponse({})
