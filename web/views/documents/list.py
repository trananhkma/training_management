import logging

from django.views.generic.base import TemplateView
from django.shortcuts import render

from web.utils import get_course, get_document
from web.form import DocumentForm


logger = logging.getLogger(__name__)


class DocumentView(TemplateView):

    template_name = 'web/documents/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = DocumentForm()

        courses = get_course()
        context['courses'] = courses

        documents = get_document()
        context['documents'] = documents
        return context

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        logger.info('===== Start searching document =====')
        title = request.POST.get('title')
        author = request.POST.get('author')
        course = request.POST.get('course')

        documents = get_document(title__icontains=title, author__icontains=author, course=course)
        params = {'title': title, 'author': author, 'course': course}
        logger.info('Criteria: {}'.format(params))

        courses = get_course()

        context['form'] = DocumentForm(initial={'title': title, 'author': author})
        context['course'] = course
        context['courses'] = courses
        context['documents'] = documents

        logger.info('===== Finish searching document =====')
        return render(request, self.template_name, context)

    def handle_uploaded_file(self, file, path):
        with open(path, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)