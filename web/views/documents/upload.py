import logging
import os

from django.conf import settings
from django.contrib import messages
from django.views.generic.base import TemplateView
from django.shortcuts import redirect, render
from django.db import IntegrityError

from web.utils import get_course, get_courses_from_user, add_document
from web.form import DocumentUpload


logger = logging.getLogger(__name__)


class DocumentUploadView(TemplateView):

    template_name = 'web/documents/upload.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = DocumentUpload()

        if self.request.user.full_name == 'admin':
            courses = get_course()
        else:
            courses = get_courses_from_user(user_id=self.request.user.employee_id)

        if not courses:
            messages.error(self.request, 'You do not join any course yet')
            context['can_upload'] = False
        else:
            context['can_upload'] = True
        context['courses'] = courses

        return context

    def post(self, request, *args, **kwargs):
        logger.info('===== Start uploading new document =====')
        title = request.POST.get('title')
        author = request.POST.get('author')
        fileobj = request.FILES['file']
        course = request.POST.get('course')

        save_path = os.path.join(settings.MEDIA_ROOT, 'uploads', fileobj.name)

        self.handle_uploaded_file(fileobj, save_path)
        logger.info('Document name: {}'.format(fileobj.name))

        try:
            add_document(course=course, title=title, author=author, file_name=fileobj.name,
                         uploaded_by='{} ({})'.format(request.user.full_name, request.user.employee_id))
        except IntegrityError as e:
            logger.info('===== {} ====='.format(e))
            messages.error(request, 'Duplicated Title')
            context = self.get_context_data()
            context.update({
                'form': DocumentUpload(initial={'title': title, 'author': author}),
                'course': course
            })
            return render(request, self.template_name, context)

        logger.info('===== Finish uploading new document =====')
        messages.success(request, 'Uploaded document {} successfully'.format(fileobj))
        return redirect('web:document_list')

    def handle_uploaded_file(self, file, path):
        with open(path, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)