import os

from django.conf import settings
from django.http import HttpResponse, Http404
from web.utils import get_document


def download(request, doc_id):
    file_name = get_document(id=doc_id)[0][3]
    path = os.path.join(settings.MEDIA_ROOT, 'uploads', file_name)
    if os.path.exists(path):
        with open(path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(path)
            return response
    raise Http404