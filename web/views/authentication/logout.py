import logging

from django.views.generic.base import TemplateView
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth import logout


logger = logging.getLogger(__name__)


class Logout(TemplateView):

    def get(self, request, **kwargs):
        logger.info('===== User {} logged out ====='.format(request.user.username))
        logout(request)
        messages.success(request, 'Logged out')
        return redirect('web:web-index')
