import logging

from django.contrib import messages
from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login

from web.form import LoginForm


logger = logging.getLogger(__name__)


class Login(TemplateView):

    template_name = "registration/login.html"

    def get(self, request, **kwargs):
        context = super().get_context_data(**kwargs)
        next = request.GET.get('next', '/')
        request.session['next'] = next
        context['form'] = LoginForm()
        context['next'] = next
        if request.user.is_authenticated:
            messages.error(request, 'You should logout first!')
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        account = request.POST.get('account')
        password = request.POST.get('password')
        next = request.GET.get('next')
        if not next:
            next = 'web:web-index'
        logger.info('===== User {} is logging in ====='.format(account))
        user = authenticate(request, username=account, password=password)
        if not user:
            messages.error(request, 'Wrong account or password')
            logger.error('===== User {} failed to login ====='.format(account))
            context['form'] = LoginForm(initial={'account': account})
            return render(request, self.template_name, context)

        login(request, user)

        logger.info('===== User {} logged in successfully ====='.format(account))
        messages.success(request, 'Logged in')
        return redirect(next)
