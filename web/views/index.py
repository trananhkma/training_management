from django.views.generic.base import TemplateView
from django.shortcuts import render


class Index(TemplateView):

    template_name = 'web/index.html'

    def get(self, request, **kwargs):
        return render(request, self.template_name, context={'user': request.user.username })