import logging

from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import IntegrityError
from django.core.exceptions import ValidationError
from django.contrib.auth.mixins import PermissionRequiredMixin

from web.form import CourseManagementForm
from web.utils import get_user, get_course, get_user_from_course, update_course


logger = logging.getLogger(__name__)


class CourseManagement(PermissionRequiredMixin, TemplateView):

    template_name = "web/courses/management.html"
    permission_required = 'web.manage_courses'

    def handle_no_permission(self):
        messages.error(self.request, "You don't have permission to access this page")
        return render(self.request, 'permission_denied.html')

    def get(self, request, **kwargs):
        context = super().get_context_data(**kwargs)
        course_id = kwargs['course_id']
        course = get_course(id=course_id)
        if not course:
            messages.error(request, 'Course not found')
            return redirect('web:course_list')

        course = course[0]
        form = CourseManagementForm(initial={'id': course[0], 'name': course[1], 'from_date': course[2], 'to_date': course[3]}, request=request)
        users = get_user()
        logger.info('users: {}'.format(users))
        joined_users = get_user_from_course(course_id=course[0])
        logger.info('joined_users: {}'.format(joined_users))
        joined_user_id = [str(i.id) for i in joined_users]
        logger.info('joined_user_id: {}'.format(joined_user_id))

        context['form'] = form
        context['users'] = users
        context['joined_user_id'] = joined_user_id
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        course = kwargs['course_id']
        context = self.get_context_data(**kwargs)
        logger.info('===== Updating Course {} ====='.format(course))

        name = request.POST.get('name')
        start_date = request.POST.get('from_date')
        end_date = request.POST.get('to_date')
        users = request.POST.getlist('users')

        if not start_date:
            start_date = None

        if not end_date:
            end_date = None

        form = {'id': course, 'name': name, 'from_date': start_date, 'to_date': end_date}
        logger.info('Data: {}'.format({**form, 'users': users}))

        try:
            update_course(course, name, start_date, end_date, users)
        except IntegrityError as e:
            logger.error('===== {} ====='.format(e))
            messages.error(request, 'Duplicated Course Name')

            context['form'] = CourseManagementForm(initial=form, request=self.request)
            context['joined_user_id'] = users
            return render(request, self.template_name, context=context)
        except ValidationError as e:
            logger.error('===== {} ====='.format(e))
            messages.error(request, str(e)[2:-2])
            context['form'] = CourseManagementForm(initial=form, request=self.request)
            context['joined_user_id'] = users

            return render(request, self.template_name, context)

        messages.success(request, 'Updated data successfully')
        logger.info('===== Finish updating Course =====')
        return redirect('web:course_list')
