import logging

from django.contrib import messages
from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.db import IntegrityError
from django.core.exceptions import ValidationError
from django.contrib.auth.mixins import PermissionRequiredMixin

from web.utils import add_course
from web.form import CourseAddForm


logger = logging.getLogger(__name__)


class CourseAdd(PermissionRequiredMixin, TemplateView):

    template_name = "web/courses/add.html"
    permission_required = 'web.add_courses'

    def handle_no_permission(self):
        messages.error(self.request, "You don't have permission to access this page")
        return render(self.request, 'permission_denied.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CourseAddForm()
        return context

    def post(self, request, *args, **kwargs):
        logger.info('===== Start adding new course =====')
        context = super().get_context_data(**kwargs)
        name = request.POST.get('name')
        from_date = request.POST.get('from_date')
        to_date = request.POST.get('to_date')

        if not from_date:
            from_date = None

        if not to_date:
            to_date = None

        form = {'name': name, 'from_date': from_date, 'to_date': to_date}

        logger.info(form)

        try:
            add_course(name=name, start_date=from_date, end_date=to_date)
        except IntegrityError as e:
            logger.error('===== {} ====='.format(e))
            messages.error(request, 'Duplicated Course Name')

            context['form'] = CourseAddForm(initial=form)
            return render(request, self.template_name, context=context)
        except ValidationError as e:
            logger.error('===== {} ====='.format(e))
            messages.error(request, str(e)[2:-2])

            context['form'] = CourseAddForm(initial=form)
            return render(request, self.template_name, context=context)


        logger.info('===== Finish adding new course =====')
        messages.success(request, 'Course {} has been added successfully'.format(name))
        return redirect('web:course_list')
