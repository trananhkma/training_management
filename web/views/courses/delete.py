import logging

from django.contrib import messages
from django.http import HttpResponseForbidden
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

from web.utils import remove_course


logger = logging.getLogger(__name__)


def delete_course(request, course_id):
    logger.info('==== Start deleting course {} ====='.format(course_id))
    if not request.user.has_perm('web.delete_courses'):
        logger.error('==== {} ====='.format('Permission Denied'))
        return HttpResponseForbidden('<H1>Permission denied!</H1>')
    try:
        remove_course(id=course_id)
        logger.info('==== Finish deleting course =====')
        messages.success(request, 'Deleted course successfully')
    except ObjectDoesNotExist as e:
        logger.error('==== {} ====='.format(e))
        messages.error(request, 'Course not found')
    return JsonResponse({})
