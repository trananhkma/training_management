import logging

from django.views.generic.base import TemplateView
from django.shortcuts import render
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin

from web.form import CourseForm
from web.utils import get_course


logger = logging.getLogger(__name__)


class CourseView(PermissionRequiredMixin, TemplateView):

    template_name = "web/courses/list.html"
    permission_required = 'web.search_courses'

    def handle_no_permission(self):
        messages.error(self.request, "You don't have permission to access this page")
        return render(self.request, 'permission_denied.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CourseForm()
        courses = get_course()
        context['courses'] = courses
        return context

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        logger.info('===== Start searching Course =====')
        name = request.POST.get('name')
        id = request.POST.get('id')
        from_date = request.POST.get('from_date')
        to_date = request.POST.get('to_date')

        criteria = {'name': name, 'id': id, 'from_date':from_date, 'to_date': to_date}
        logger.info('Criteria: {}'.format(criteria))
        try:
            if from_date or to_date:
                _from_date = from_date if from_date else '1980-1-1'
                _to_date = to_date if to_date else '2100-1-1'
                courses = get_course(id=id, name__icontains=name, start_date__range=[_from_date, _to_date])
            else:
                courses = get_course(id=id, name__icontains=name)
            context['courses'] = courses
        except ValidationError as e:
            logger.error('===== {} ====='.format(e))
            messages.error(request, str(e)[2:-2])
            courses = []

        logger.info('Record found: {}'.format(len(courses)))
        logger.info('===== Finish searching Course =====')

        form = CourseForm(initial=criteria)
        context['form'] = form

        return render(request, self.template_name, context=context)
