from django.conf.urls import url
from django.contrib.auth.decorators import login_required


from web.views.index import Index
from web.views.authentication.login import Login
from web.views.authentication.logout import Logout

from web.views.users.list import UserView
from web.views.users.add import UserAdd
from web.views.users.delete import delete_user
from web.views.users.management import UserManagement
from web.views.users.evaluate import UserEvaluate

from web.views.courses.list import CourseView
from web.views.courses.add import CourseAdd
from web.views.courses.delete import delete_course
from web.views.courses.management import CourseManagement

from web.views.documents.upload import DocumentUploadView
from web.views.documents.list import DocumentView
from web.views.documents.download import download
from web.views.documents.delete import delete_document


app_name = 'web'


urlpatterns = [
    url(r'^$', login_required(Index.as_view()), name="web-index"),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^user/$', login_required(UserView.as_view()), name="user_list"),
    url(r'^user/add/$', login_required(UserAdd.as_view()), name="user_add"),
    url(r'^user/delete/(?P<user_id>[0-9A-Za-z]+)$', delete_user, name="user_delete"),
    url(r'^user/management/(?P<user_id>[0-9A-Za-z]+)$', login_required(UserManagement.as_view()), name="user_management"),
    url(r'^course/$', login_required(CourseView.as_view()), name="course_list"),
    url(r'^course/add/$', login_required(CourseAdd.as_view()), name="course_add"),
    url(r'^course/delete/(?P<course_id>[0-9A-Za-z]+)$', delete_course, name="course_delete"),
    url(r'^course/management/(?P<course_id>[0-9A-Za-z]+)$', login_required(CourseManagement.as_view()), name="course_management"),
    url(r'^course/evaluation/(?P<course_id>[0-9A-Za-z]+)/(?P<user_id>[0-9A-Za-z]+)$', login_required(UserEvaluate.as_view()), name="evaluate_user"),
    url(r'^document/$', login_required(DocumentView.as_view()), name="document_list"),
    url(r'^document/upload/$', login_required(DocumentUploadView.as_view()), name="document_upload"),
    url(r'^document/download/(?P<doc_id>[0-9A-Za-z]+)$', download, name="document_download"),
    url(r'^document/delete/(?P<doc_id>[0-9A-Za-z]+)$', delete_document, name="document_delete"),
]
