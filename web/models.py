from django.db import models
from django.contrib.auth.models import AbstractUser


TEACHER = 1
STUDENT = 0
ROLE_CHOICES = (
    (STUDENT, 'student'),
    (TEACHER, 'teacher'),
)


class User(AbstractUser):
    employee_id = models.IntegerField(unique=True, null=True)
    full_name = models.CharField(max_length=255, null=True)
    role = models.SmallIntegerField(choices=ROLE_CHOICES, null=True)
    department = models.CharField(max_length=255, null=True)

    def get_role(self):
        return ROLE_CHOICES[int(self.role)][1]

    class Meta:
        permissions = (
            ('manage_users', 'Can manage users'),
        )


class Courses(models.Model):
    name = models.CharField(max_length=255, unique=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    users = models.ManyToManyField(User)

    class Meta:
        permissions = (
            ('manage_courses', 'Can manage courses'),
            ('search_courses', 'Can search courses'),
        )


class Documents(models.Model):
    title = models.CharField(max_length=255, unique=True)
    author = models.CharField(max_length=255, null=True)
    file_name = models.CharField(max_length=255)
    uploaded_by = models.CharField(max_length=255, null=True)
    course = models.ForeignKey(Courses, on_delete=models.CASCADE, to_field='name')

    class Meta:
        permissions = (
            ('download_documents', 'Can download documents'),
            ('search_documents', 'Can search documents'),
        )


class Evaluation(models.Model):
    class Meta:
        unique_together = (('student_id'), ('course_name'))
        permissions = (
            ('view_evaluation', 'Can view evaluation'),
        )

    student_id = models.IntegerField(null=True)
    course_name = models.CharField(max_length=255, null=True)
    assessor_id = models.IntegerField(null=True)
    assessor_name = models.CharField(max_length=255, null=True)
    evaluation = models.TextField(null=True)
