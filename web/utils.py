from web.models import User, Courses, Documents, Evaluation
from django.contrib.auth.models import Group


def get_user(**kwargs):
    if not kwargs:
        query = User.objects.all()
    else:
        kwargs = {k:kwargs[k] for k in kwargs if kwargs[k] != ''}
        query = User.objects.filter(**kwargs)

    return list(query.values_list())

def add_user(password, **kwargs):
    user = User(**kwargs)
    gr = user.get_role()
    user.set_password(password)
    user.save()
    group = Group.objects.get(name=gr)
    user.groups.add(group)

def remove_user(**kwargs):
    user = User.objects.get(**kwargs)
    user.delete()

def get_course(**kwargs):
    if not kwargs:
        query = Courses.objects.all()
    else:
        kwargs = {k:kwargs[k] for k in kwargs if kwargs[k]}
        query = Courses.objects.filter(**kwargs)

    return list(query.values_list())

def remove_course(**kwargs):
    course = Courses.objects.get(**kwargs)
    course.delete()

def add_course(**kwargs):
    course = Courses(**kwargs)
    course.save()

def add_user_to_course(course_id, user_mod):
    course = Courses.objects.get(id=course_id)
    course.users.add(user_mod)

def get_courses_from_user(user_id):
    user = User.objects.get(employee_id=user_id)
    return list(user.courses_set.values_list())

def get_user_from_course(course_id):
    course = Courses.objects.get(id=course_id)
    return list(course.users.all())

def update_user(user_id, department, courses):
    user = User.objects.get(employee_id=user_id)
    user.department = department
    user.courses_set.clear()
    for course in courses:
        add_user_to_course(course, user)
    user.save()

def add_document(course, **kwargs):
    courseobj = Courses.objects.get(id=course)
    doc = Documents(**kwargs)
    doc.course = courseobj
    doc.save()

def get_document(**kwargs):
    if not kwargs:
        query = Documents.objects.all()
    else:
        kwargs = {k:kwargs[k] for k in kwargs if kwargs[k]}
        query = Documents.objects.filter(**kwargs)

    return list(query.values_list())

def remove_document(**kwargs):
    doc = Documents.objects.get(**kwargs)
    doc.delete()

def update_course(course_id, name, start_date, end_date, users):
    course = Courses.objects.get(id=course_id)
    course.name = name
    course.start_date = start_date
    course.end_date = end_date
    course.users.clear()
    for s in users:
        user = User.objects.get(id=s)
        add_user_to_course(course_id, user)
    course.save()

def add_evaluation(student_id, assessor_id, assessor_name, course_id, evaluation):
    course = get_course(id=course_id)[0]
    e, created  = Evaluation.objects.update_or_create(student_id=student_id, course_name=course[1],
                                                      defaults={'assessor_id': assessor_id,
                                                                'assessor_name': assessor_name,
                                                                'evaluation':evaluation})
    e.save()

def get_evaluation(**kwargs):
    if not kwargs:
        query = Evaluation.objects.all()
    else:
        kwargs = {k:kwargs[k] for k in kwargs if kwargs[k] != ''}
        query = Evaluation.objects.filter(**kwargs)

    return list(query.values_list())