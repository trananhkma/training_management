from django import forms


CHOICES = [('1', 'Teacher'), ('0', 'Student')]


class UserForm(forms.Form):
    id = forms.IntegerField(label='Employee ID', required=False, widget=forms.NumberInput(attrs={'max': 2147483647, 'min':0}))
    name = forms.CharField(label='User Name', required=False, max_length=255)


class UserAddForm(forms.Form):
    id = forms.IntegerField(label='Employee ID *', widget=forms.NumberInput(attrs={'placeholder': '', 'max': 2147483647, 'min':0}))
    name = forms.CharField(label='User Name *', max_length=255, widget=forms.TextInput(attrs={'placeholder': ''}))
    department = forms.CharField(required=False, max_length=255, widget=forms.TextInput(attrs={'placeholder': ''}))
    account = forms.CharField(label='Account *', max_length=255, widget=forms.TextInput(attrs={'placeholder': ''}))
    password = forms.CharField(label='Password *', widget=forms.PasswordInput(attrs={'placeholder': ''}))
    role = forms.ChoiceField(label='Role *', widget=forms.RadioSelect, choices=CHOICES)


class UserManagementForm(forms.Form):
    id = forms.IntegerField(label='Employee ID', disabled=True)
    name = forms.CharField(label='User Name', max_length=255, disabled=True)
    department = forms.CharField(label='Department', required=False, max_length=255)


class CourseManagementForm(forms.Form):
    def __init__(self, request, *args, **kwargs):
        super(CourseManagementForm, self).__init__(*args, **kwargs)
        self.fields['id'] = forms.IntegerField(label='ID', disabled=True)
        self.fields['name'] = forms.CharField(label='Name', max_length=255)
        self.fields['from_date'] = forms.DateField(label='Start Date', required=False)
        self.fields['to_date'] = forms.DateField(label='End Date', required=False)

        if not request.user.has_perm('user.change_courses'):
            self.fields['name'].disabled = True
            self.fields['from_date'].disabled = True
            self.fields['to_date'].disabled = True


class CourseForm(forms.Form):
    id = forms.IntegerField(label='Course ID', required=False, widget=forms.NumberInput(attrs={'max': 2147483647, 'min':0}))
    name = forms.CharField(label='Course Name', required=False, max_length=255)
    from_date = forms.DateField(label='Start Date From', required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    to_date = forms.DateField(label='Start Date To', required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'}))


class CourseAddForm(forms.Form):
    name = forms.CharField(label='Course Name *', max_length=255, widget=forms.TextInput(attrs={'placeholder': ''}))
    from_date = forms.DateField(label='Start Date', required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    to_date = forms.DateField(label='End Date', required=False, widget=forms.widgets.DateInput(attrs={'type': 'date'}))


class DocumentUpload(forms.Form):
    title = forms.CharField(max_length=255, label='Title *', widget=forms.TextInput(attrs={'placeholder': ''}))
    author = forms.CharField(max_length=255, required=False, widget=forms.TextInput(attrs={'placeholder': ''}))
    file = forms.FileField()


class DocumentForm(forms.Form):
    title = forms.CharField(max_length=255, required=False)
    author = forms.CharField(max_length=255, required=False)


class EvaluateForm(forms.Form):
    course = forms.CharField(max_length=255, disabled=True)
    student = forms.CharField(max_length=255, disabled=True)


class LoginForm(forms.Form):
    account = forms.CharField(max_length=255)
    password = forms.CharField(widget=forms.PasswordInput)