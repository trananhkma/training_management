from django.contrib.auth.models import Group, Permission
from web.models import User

manage_users = Permission.objects.get(codename='manage_users')
add_user = Permission.objects.get(codename='add_user')
delete_user = Permission.objects.get(codename='delete_user')
edit_user = Permission.objects.get(codename='change_user')

manage_courses = Permission.objects.get(codename='manage_courses')
add_courses = Permission.objects.get(codename='add_courses')
delete_courses = Permission.objects.get(codename='delete_courses')
edit_courses = Permission.objects.get(codename='change_courses')
search_courses = Permission.objects.get(codename='search_courses')

add_documents = Permission.objects.get(codename='add_documents')
delete_documents = Permission.objects.get(codename='delete_documents')
download_documents = Permission.objects.get(codename='download_documents')
search_documents = Permission.objects.get(codename='search_documents')

view_evaluation = Permission.objects.get(codename='view_evaluation')
add_evaluation = Permission.objects.get(codename='add_evaluation')


gr_teacher = Group.objects.create(name='teacher')
gr_student = Group.objects.create(name='student')


gr_teacher.permissions.add(search_courses)
gr_teacher.permissions.add(manage_courses)
gr_teacher.permissions.add(view_evaluation)
gr_teacher.permissions.add(add_evaluation)
gr_teacher.permissions.add(delete_documents)
gr_teacher.permissions.add(add_documents)
gr_teacher.permissions.add(download_documents)
gr_teacher.permissions.add(search_documents)

gr_student.permissions.add(search_documents)
gr_student.permissions.add(add_documents)
gr_student.permissions.add(download_documents)


admin = User.objects.get(username='admin')
admin.objects.update(full_name='admin', employee_id=1)
admin.save()